import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-content',
  templateUrl: './landing-content.component.html',
  styleUrls: ['./landing-content.component.scss']
})
export class LandingContentComponent implements OnInit {

  public contentOne?: Icontent;
  public contentTwo?: Icontent;
  constructor() {
    this.contentOne = {
      title: 'Hola Upgrade',
        tags: 'Hola Upgrade',
    content: 'Hola Upgrade',
    PersonalInfo: 'Hola Upgrade',
    }
  }

  ngOnInit(): void {
  }

}
