import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.scss']
})
export class LandingHeaderComponent implements OnInit {

  public headerList;
  constructor() {
    this.headerList = ['home', 'content', 'about'];
  }

  ngOnInit(): void {
  }

}
