import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './pages/landing/landing.component';
import { LandingHeaderComponent } from './pages/landing/landing-header/landing-header.component';
import { LandingContentComponent } from './pages/landing/landing-content/landing-content.component';
import { LandingFooterComponent } from './pages/landing/landing-footer/landing-footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LandingHeaderComponent,
    LandingContentComponent,
    LandingFooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
