document.addEventListener('DOMContentLoaded', (event) => {

    async function getPokemon(pokemonName) {
        try {
            const respuesta = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
            debugger;
            const resultado = await respuesta.json();
            //console.log(resultado)
            const { id, name, sprites: { other: { dream_world: { front_default } } } } = resultado;


            if (document.querySelector(".pokemon-photo")) {
                document.querySelector(".pokemon-photo").remove()
            }

            if (document.querySelector(".pokemon-box__data-container")) {
                document.querySelector(".pokemon-box__data-container").remove();
            }

            let pokemonBox = document.querySelector(".pokemon-box")
            let pokemonDataContainer = document.createElement("div");
            pokemonDataContainer.classList.add("pokemon-box__data-container");
            let htmlName = document.createElement("p");
            htmlName.innerHTML = "Nombre: " + name;
            let htmlOrder = document.createElement("p");
            htmlOrder.innerHTML = "Número: " + id;
            pokemonDataContainer.appendChild(htmlName);
            pokemonDataContainer.appendChild(htmlOrder);
            pokemonBox.appendChild(pokemonDataContainer);
            let divImage = document.querySelector(".pokemon-box__img-container");
            let img = document.createElement("img");
            img.setAttribute("src", front_default);
            img.classList.add("pokemon-photo");
            let divData = document.querySelector(".pokemon-box__data-container");
            divData.appendChild(htmlName);
            divData.appendChild(htmlOrder);
            divImage.appendChild(img);

        } catch {
            alert("No se conoce ningún Pokemon con ese nombre. Prueba con otro.")
        }

    }

    document.getElementById("send-pokemon-name").addEventListener("click", function() {
        let pokemonName = document.getElementById("pokemon-name").value;
        getPokemon(pokemonName);
      });

});



