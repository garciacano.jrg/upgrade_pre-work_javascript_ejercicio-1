

function ver(arg) {
    if (arg === "studies") {
        var element = document.getElementById('studies');
        element.classList.add("active");
        document.getElementById('experience').classList.remove("active");
        document.getElementById('idiomas').classList.remove("active");
        document.getElementById('actitudes').classList.remove("active");
    } else if (arg === "experience") {
        var element = document.getElementById('experience');
        element.classList.add("active");
        document.getElementById('studies').classList.remove("active");
        document.getElementById('idiomas').classList.remove("active");
        document.getElementById('actitudes').classList.remove("active");
    } else if (arg === "actitudes") {
        var element = document.getElementById('actitudes');
        element.classList.add("active");
        document.getElementById('studies').classList.remove("active");
        document.getElementById('idiomas').classList.remove("active");
        document.getElementById('experience').classList.remove("active");
    } else if (arg === "idiomas") {
        var element = document.getElementById('idiomas');
        element.classList.add("active");
        document.getElementById('studies').classList.remove("active");
        document.getElementById('experience').classList.remove("active");
        document.getElementById('actitudes').classList.remove("active");
    }
}

function imprimirNombre() {
    var name = document.getElementById("name").innerHTML;
    for (var i = 0; i < 100; i++) {
        console.log((i+1) + ": " + name);
    }
}